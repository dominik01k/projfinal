package ciai.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Student {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
	
    private String name;
	private String studentnumber;
	private String email;
	private String uni_email;
	private String degree;
	private String photo;
	private String password;
	private String addres;
	private String currentCourse;

	
	
	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnore	
	private Set<Enrollment> courses;
	
	
	public Student() {}
	
	public Student(String name, String student_number,String email, String uni_email, String degree, String photo, String password, String addres) {
		this.setName(name);
		this.setStudentnumber(student_number);
		this.setEmail(email);
		this.setUni_email(uni_email);
		this.setDegree(degree);
		this.setPhoto(photo);
		this.setPassword(password);
		this.setAddres(addres);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddres() {
		return addres;
	}

	public void setAddres(String addres) {
		this.addres = addres;
	}



	public String getUni_email() {
		return uni_email;
	}

	public void setUni_email(String uni_email) {
		this.uni_email = uni_email;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStudentnumber() {
		return studentnumber;
	}

	public void setStudentnumber(String studentnumber) {
		this.studentnumber = studentnumber;
	}

	public String getCurrentCourse() {
		return currentCourse;
	}

	public void setCurrentCourse(String currentCourse) {
		this.currentCourse = currentCourse;
	}
	
	public Set<Enrollment> getCourses() {
		return courses;
	}

	public void setCourses(Set<Enrollment> courses) {
		this.courses = courses;
	}

	
}
