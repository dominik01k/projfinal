package ciai.model;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface CourseRepository extends CrudRepository<Course, Long> {
	
	Course findByNumber(String number);
	List<Course> findByName(String name);
	
  
   
}


