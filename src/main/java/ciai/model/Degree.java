package ciai.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Degree {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;	
    private String name;	
	private String description;
	private int ects;
	private int years;
	private String courses;
	
	
	public Degree() {}
	
	public Degree(long id,String name, String description, int ects,int years, String courses) {
		this.setId(id);
		this.setName(name);
		this.setDescription(description);
		this.setEcts(ects);
		this.setYears(years);
		this.setCourses(courses);
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getEcts() {
		return ects;
	}

	public void setEcts(int ects) {
		this.ects = ects;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	public String getCourses() {
		return courses;
	}

	public void setCourses(String courses) {
		this.courses = courses;
	}

	
}
