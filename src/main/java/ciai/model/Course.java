package ciai.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "course")
public class Course {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
	
    private String number;
	private String name;
	private String description;
	private int ects;
	private String edition;
	private int closed;
	private String prof;
	
	@ManyToMany //(mappedBy = "courses")
	@JsonIgnore
	private Set<Professor> professors;
	
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<Enrollment> enrollments;
	
	
	public Course() {}
	
	public Course(String number, String name, String description, int ects,String edition, int closed) {
		//.setId(id);
		this.setNumber(number);
		this.setName(name);
		this.setDescription(description);
		this.setEcts(ects);		
		this.setEdition(edition);
		this.setClosed(closed);
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getEcts() {
		return ects;
	}

	public void setEcts(int ects) {
		this.ects = ects;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public int getClosed() {
		return closed;
	}

	public void setClosed(int closed) {
		this.closed = closed;
	}
	
	public Set<Enrollment> getEnrollment() {
		return enrollments;
	}

	public void setEnrollment(Set<Enrollment> enrollments) {
		this.enrollments = enrollments;
	}

	public String getProf() {
		return prof;
	}

	public void setProf(String prof) {
		this.prof = prof;
	}
	

}
