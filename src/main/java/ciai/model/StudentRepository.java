package ciai.model;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface StudentRepository extends CrudRepository<Student, Long> {
	List<Student> findByStudentnumber(String number);
	
	List<Student> findByName(String name);
  
    @Query("select s from Student s where s.name like CONCAT(?,'%')")
    List<Student> search(String name);
    
    @Query(" select s from Student s "+
      	   "   inner join s.courses en "+
      	   " where en.course.id = :id")
      List<Student> findByCourse(@Param("id") long id);
    
    
    
}
