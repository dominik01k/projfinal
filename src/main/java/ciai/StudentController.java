package ciai;

import static org.mockito.Matchers.startsWith;


import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import javax.swing.plaf.synth.SynthStyleFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ciai.model.Course;
import ciai.model.CourseRepository;
import ciai.model.Enrollment;
import ciai.model.Student;
import ciai.model.StudentRepository;

@Controller
@RequestMapping(value = "/students")
public class StudentController {
	
	
	@Autowired
	StudentRepository students;
	@Autowired
	CourseRepository courses;

	
	@RequestMapping(value = "")
	public @ResponseBody Iterable<Student> getStudents() {	
		return students.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Student getStudent(@PathVariable long id) {
		return students.findOne(id);
	}

	@RequestMapping(value= "", method=RequestMethod.POST)
	public @ResponseBody long addStudent(@RequestBody Student student) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		student.setPassword(encoder.encode(student.getPassword()));
		System.out.println(student.getId());
		students.save(student);
		return student.getId();
	}
	
	@RequestMapping(value= "", method=RequestMethod.PUT)
	public @ResponseBody Student updateStudent(@RequestBody Student student) {
		//System.out.println("student number");
		Student stud = students.findOne(student.getId());
		stud.setAddres(student.getAddres());
		stud.setEmail(student.getEmail());
		stud.setStudentnumber(student.getStudentnumber());
		students.save(stud);
		return stud;
	}
	
	@RequestMapping(value = "/profile/{student_number}", method = RequestMethod.GET)
	public @ResponseBody Student findByUsername(@PathVariable String student_number) {
		return  students.findByStudentnumber(student_number).get(0);
	}
	

	@RequestMapping(value = "/profile/{number}/courses", method = RequestMethod.GET)
	public @ResponseBody Set<String> find(@PathVariable String number) {
		Set<String> coursesSet = students.findByStudentnumber(number).get(0).getCourses().stream().map(c -> c.getCourse().getName()).collect(Collectors.toSet());
		return coursesSet;
	}


}
