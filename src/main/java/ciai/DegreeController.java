package ciai;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ciai.model.Degree;
import ciai.model.DegreeRepository;

@Controller
@RequestMapping(value = "/degrees")
public class DegreeController {
	
	@Autowired
	DegreeRepository degrees;
	
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody Iterable<Degree> getDegrees() {			
		return degrees.findAll();
	}
	
	
}
