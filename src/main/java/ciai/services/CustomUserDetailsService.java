package ciai.services;

import java.util.LinkedList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import ciai.model.ProfessorRepository;
import ciai.model.StudentRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	StudentRepository students;
	@Autowired
	ProfessorRepository proffesors;
	
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<GrantedAuthority> authorities = new LinkedList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER")); // can also be obtained from a repository		
		if(username.chars().allMatch(Character::isDigit)){
			System.out.println("bol som  tu");
			authorities.add(new SimpleGrantedAuthority("ROLE_STUDENT"));
		}
		else{		
			authorities.add(new SimpleGrantedAuthority("ROLE_PROFESSOR"));
		}
		List<ciai.model.Student> studentsList = students.findByStudentnumber(username);
		if (studentsList.isEmpty()){
			List<ciai.model.Professor> proffList = proffesors.findByUsername(username);
			if(proffList.isEmpty()){
				return new User(username,"ehsptgsijtvoijtlhflbhjgvkf",authorities);
			}
			return new User(username, proffList.get(0).getPassword(), authorities);
		}
		return new User(username, studentsList.get(0).getPassword(), authorities);
	}

}