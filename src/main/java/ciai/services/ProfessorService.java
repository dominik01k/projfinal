package ciai.services;

import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ciai.model.Course;
import ciai.model.CourseRepository;
import ciai.model.Professor;
import ciai.model.ProfessorRepository;

@Component
public class ProfessorService {

	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	ProfessorRepository professors;
	
	@Transactional
	public void addCourses(String username, String... courses) {
		Professor p = professors.findByUsername(username).get(0);
		
		Set<Course> cs = p.getCourses();
		for(String c: courses)
			cs.add(courseRepository.findByName(c).get(0));
		professors.save(p);
	}

	@Transactional
	public Set<String> getCourses(long id) {
		return professors.findOne(id).getCourses().stream().map( c -> c.getName()).collect(Collectors.toSet());
	}

}