package ciai;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

import ciai.model.Course;
import ciai.model.CourseRepository;
import ciai.model.Student;
import ciai.model.StudentRepository;
import ciai.model.Degree;
import ciai.model.DegreeRepository;
import ciai.model.Enrollment;
import ciai.model.Professor;
import ciai.model.ProfessorRepository;
import ciai.services.ProfessorService;




@SpringBootApplication
@EnableSpringDataWebSupport
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ClassroomApplication {

	private static final Logger log = LoggerFactory.getLogger(ClassroomApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ClassroomApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner init(StudentRepository repository, CourseRepository repositoryC, DegreeRepository repositoryD, ProfessorRepository repositoryP, ProfessorService profService) {
		return (args) -> {
			
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			Student[] students =
				{new Student("Ingrid Daubechies", "5555", "ingrid@gmail.com","IngridDaubechies@fct.unl.pt","Informatics","avatar1.png",encoder.encode("password"), "Rua de Rui Cunhada 42, Lisbon"),
					new Student("Jane Goodall","5556", "jane@gmail.com","JaneGoodall@fct.unl.pt","Informatics","avatar2.png",encoder.encode("password"), "Rua de Rui Cunhada 45, Lisbon"),
					new Student("Veronika Pohorencova","5557", "pohi@gmail.com","veronikapohorencova@fct.unl.pt","Informatics","female.jpg",encoder.encode("password"), "Rua de Rui Cunhada 47, Lisbon"),
					new Student("Dominik Knechta","5558", "dominik@gmail.com","dominikknechta@fct.unl.pt","Mathematics","clip2.png",encoder.encode("password"), "Rua de Rui Cunhada 48, Lisbon"),
					new Student("Janko Mrkvicka","5559", "janko@gmail.com","jankomrkvicka@fct.unl.pt","Mathematics","avatar1.png",encoder.encode("password"), "Rua de Rui Cunhada 49, Lisbon")	
				};
		
			for(Student s: students) repository.save(s);
			
			Course[] courses =
				{new Course("LK4508","Mathematic","Bacon ipsum dolor amet kielbasa doner shank fatback drumstick, ground round rump beef pork loin.",12, "summer",0),
				 new Course("ZK5508","Biology","capicola pancetta. Capicola sausage meatloaf, pork chop landjaeger pancetta biltong flank. ",14, "winter", 1),
				 new Course("LR4408","Chemistry","capicola pancetta. Capicola sausage meatloaf, pork chop landjaeger pancetta biltong flank.", 8, "winter",1),
				 new Course("LM4568","Graphics","capicola pancetta. Capicola sausage meatloaf, pork chop landjaeger pancetta biltong flank.", 6, "winter",1),
				 new Course("KR45678","Graphics 2","capicola pancetta. Capicola sausage meatloaf, pork chop landjaeger pancetta biltong flank.", 5, "summer",1)
				};
		
			for(Course c: courses) repositoryC.save(c);
			
			
			Degree[] degrees =
				{new Degree(1L,"Computer science","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 15.",120,4, "Programming, JAVA, Python, Interactive programming..."),
				 new Degree(2L,"Mathematics","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 15.",114,3, "Algebra,Descriptive mathematics, Linear algebra,Counting to 10,Geometry..."),				 
				 new Degree(3L,"Chemistry","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 15.",78,5,"Oxigen, Aluminium ,Helium, Plutonium...")
				
				};
		
			for(Degree d: degrees) repositoryD.save(d);
			
			
			Professor[] professors =
				{new Professor(1L,"docSt",encoder.encode("password"),"Doctor Strange","BIOLOGY","professor"),
				 new Professor(2L,"docH",encoder.encode("password"),"Doctor Happy","INFORMATICS","professor"),
				 new Professor(3L,"docS",encoder.encode("password"),"Doctor Sad","MATHEMATICS","assistant")			
				};
		
			for(Professor p: professors) repositoryP.save(p);
			
			profService.addCourses("docH","Chemistry","Biology","Mathematic");			
			profService.addCourses("docS","Graphics","Graphics 2");
			profService.addCourses("docSt","Biology","Mathematic");

			Student s1 = repository.findByStudentnumber("5555").get(0);
			Student s2 = repository.findByStudentnumber("5556").get(0);
			Course c1 = repositoryC.findByNumber("LK4508");
			Course c2 = repositoryC.findByNumber("ZK5508");
			Enrollment enl1 = new Enrollment(c1,s1);
			enl1.setExam(10);
			enl1.setProject(5);
			enl1.setTest(3);
			c1.getEnrollment().add(enl1);
			c1.getEnrollment().add(new Enrollment(c1, s2));
			c2.getEnrollment().add(new Enrollment(c2, s1));

			repositoryC.save(c1);
			repositoryC.save(c2);
			
			System.out.println(c1.getEnrollment().iterator().next());
			
           
            
		};
	
	}
}
