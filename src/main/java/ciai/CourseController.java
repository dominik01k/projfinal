package ciai;


import java.util.Collections;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.security.access.prepost.PreAuthorize;

import ciai.config.AllowedToDeleteStudentFromCourse;
import ciai.model.Course;
import ciai.model.CourseRepository;
import ciai.model.Enrollment;
import ciai.model.Student;
import ciai.model.StudentRepository;
import ciai.model.Enrollment;

@Controller
@RequestMapping(value = "/courses")
public class CourseController {

	@Autowired
	CourseRepository courses;
	@Autowired
	StudentRepository students;
	
	@RequestMapping(value = "/teacherCourses", method = RequestMethod.GET)
	public @ResponseBody Iterable<Course> getCourses() {	
		
		return courses.findAll();
	}
	
	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public @ResponseBody Course findByName(@PathVariable String name) {
		System.out.println("idem najst");
		return courses.findByName(name).get(0);
	}
	
	@RequestMapping(value = "/{number}/enrolled", method = RequestMethod.GET)
	public @ResponseBody Set <Student> getEnrolled(@PathVariable String number) {
		Course c = courses.findByNumber(number);
		Set <Student> enrolled =  new HashSet<>(); 
		Set <Enrollment> en = c.getEnrollment();
		for (Enrollment e : en ){
			Student s = e.getStudent();
			s.setCurrentCourse(number);
			enrolled.add(s);
		}		
		System.out.println("enrolled students");
		System.out.println(enrolled);
		return enrolled;
	}
	
	@RequestMapping(value = "/{coursenumber}/{studentnumber}/add", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ROLE_PROFESSOR')")
	public @ResponseBody Set<Student> subscribeStudent(@PathVariable String coursenumber,@PathVariable String studentnumber) {
		Course c = courses.findByNumber(coursenumber);
		Student s = students.findByStudentnumber(studentnumber).get(0);
		System.out.println("hura");
		c.getEnrollment().add(new Enrollment(c,s));
		courses.save(c);
		Set <Student> enrolled =  new HashSet<>();
		for (Enrollment e : c.getEnrollment() ){
			Student st = e.getStudent();
			st.setCurrentCourse(coursenumber);
			enrolled.add(st);
		}	
		return enrolled;
	}
	
	
	
	@RequestMapping(value = "/{number}/enrolledMarks", method = RequestMethod.GET)
	public @ResponseBody int getEnrolledMarks(@PathVariable String number) {
		Course c = courses.findByNumber(number);
		//Set <Student> enrolled =  new HashSet<>(); 
		
		
		Set <Enrollment> en = c.getEnrollment();
		for (Enrollment e : en ){
			//Student s = e.getStudent();
			System.out.println(e.getStudent());
			System.out.println(e.getProject());
			System.out.println(e.getExam());			
		}		
		
		return 0;
	}
	
	
	
	@RequestMapping(value = "/{course}/{student}", method = RequestMethod.PUT)
	public @ResponseBody Course removeStudent(@PathVariable String course,@PathVariable String student) {
		Course c = courses.findByNumber(course);		
		Student s = students.findByStudentnumber(student).get(0);	
		Enrollment delete = new Enrollment();	
		Set <Enrollment> en = c.getEnrollment();	
		System.out.println(en);
		for (Enrollment e:en){
			if (e.getStudent() == s){
				 delete = e;
			}
		}		
		System.out.println(en.contains(delete));
		en.remove(delete);
		System.out.println(en.contains(delete));
		c.setEnrollment(en);
		courses.save(c);
		return c;
	}
	

	@RequestMapping(value = "/{id}/students", method = RequestMethod.GET)	
	public @ResponseBody List<Student> getStudents(@PathVariable String number) {
		
		return students.findByCourse(courses.findByNumber(number).getId());
	}
	
	@RequestMapping(value = "/get/{name}/{student}", method = RequestMethod.GET)	
	public @ResponseBody List<Integer> getGrades(@PathVariable String name, @PathVariable String student) {
		System.out.println(name);
		System.out.println(student);
		List<Course> c1List = courses.findByName(name);
		Course c1 = new Course();
		if(c1List.isEmpty()){
			c1 = courses.findByNumber(name);
		}
		else{
			c1= c1List.get(0);
		}
		Student s1 = students.findByStudentnumber(student).get(0);
		Set<Enrollment> enl = c1.getEnrollment();
		System.out.println(enl);
		List<Integer> grades = new ArrayList<Integer>();
		for (Enrollment enrollment : enl) {
			if(enrollment.getStudent().getId() == s1.getId()){
				System.out.println("som v znamkach");
				grades.add(enrollment.getExam());
				grades.add(enrollment.getProject());
				grades.add(enrollment.getTest());			
				
			}
		}
		return grades;
	}
	
	@RequestMapping(value = "/marks/{nameS}/{nameC}", method = RequestMethod.GET)	
	public @ResponseBody List<Integer> getMarks(@PathVariable String nameS, @PathVariable String nameC) {
		System.out.println(nameS+ " "+ nameC);
		Course c1 = courses.findByNumber(nameC);
		Student s1 = students.findByName(nameS).get(0);
		Set<Enrollment> enl = c1.getEnrollment();
		List<Integer> grades = new ArrayList<Integer>();
		for (Enrollment enrollment : enl) {
			if(enrollment.getStudent().getId() == s1.getId()){
				System.out.println("som v znamkach");
				grades.add(enrollment.getExam());
				grades.add(enrollment.getProject());
				grades.add(enrollment.getTest());			
				
			}
		}
		System.out.println("marks");
		System.out.println(grades);
		return grades;
	}

	@RequestMapping(value = "/{Sname}/{Cname}/{exam}/{project}/{test}", method = RequestMethod.PUT)	
	public @ResponseBody Course updateMarks(@PathVariable String Sname,@PathVariable String Cname,@PathVariable int exam,@PathVariable int project,@PathVariable int test) {
		System.out.println("som tu dosiel hura");
		System.out.println(exam);
		System.out.println(project);
		System.out.println(test);
		
		Course c1 = courses.findByNumber(Cname);
		Student s1 = students.findByName(Sname).get(0);
		Set<Enrollment> enl = c1.getEnrollment();
		
		for (Enrollment enrollment : enl) {
			if(enrollment.getStudent() == s1){
				System.out.println("som v znamkach");
				enrollment.setExam(exam);
				enrollment.setProject(project);
				enrollment.setTest(test);				
			}
		}
		courses.save(c1);

		return c1;
	}
	
	
}