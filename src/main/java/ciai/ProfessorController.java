package ciai;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ciai.model.Course;
import ciai.model.Professor;
import ciai.model.Student;
import ciai.model.StudentRepository;
import ciai.model.ProfessorRepository;
import ciai.model.CourseRepository;
import ciai.services.ProfessorService;

@Controller
@RequestMapping(value = "/professors")
public class ProfessorController {
	
	
	@Autowired
	ProfessorRepository professors;
	CourseRepository courses;
	StudentRepository students;
	/*@Autowired
	CourseRepository courses;*/
	ProfessorService profSerivce;
	

	@RequestMapping(value = "/{username}", method = RequestMethod.GET)
	public @ResponseBody Professor findByUsername(@PathVariable String username) {
		System.out.println("dosiel");
		System.out.println(username);
		return  professors.findByUsername(username).get(0);
	}
	
	@RequestMapping(value = "/{username}/courses", method = RequestMethod.GET)
	public @ResponseBody Set<Course> getProfessorCourses(@PathVariable String username) {
		Professor prof = professors.findByUsername(username).get(0);
		Set<Course> cs =  prof.getCourses();
		for (Course c : cs){
			c.setProf(username);
		}			
		return cs;		
	}
	
	@RequestMapping(value= "", method=RequestMethod.PUT)
	public @ResponseBody Professor updateProffesor(@RequestBody Professor professor) {
		Professor proff = professors.findOne(professor.getId());
		proff.setName(professor.getName());
		proff.setDepartment(professor.getDepartment());
		professors.save(proff);
		return proff;
	}
	

	
}
