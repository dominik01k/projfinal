/**
 * 
 */


var Dashboard = React.createClass({
	getInitialState: function() {
	    return {profCourses: []};
	  },	  
	componentDidMount: function() {	    
	    $.ajax({
		      url: "/professors/"+this.props.params.username+"/courses",
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		        this.setState({profCourses: data});
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });	  	 	    
	  },		
	render: function () {
		console.log(this.state.profCourses)
		var courseNodes = this.state.profCourses.map(function(course) {
			return (
					<DashboardCourses key={course.id} number={course.number} name={course.name} description={course.description} ects={course.ects} edition={course.edition} prof={course.prof}/>
					);
		});
		return (
                <div>                			
                	{courseNodes}    		
                </div>
               )
       }
});

var DashboardCourses = React.createClass({
	render: function(){		
		 return(
				 <div className="panel panel-danger" style={{marginLeft: 50,marginRight:50}} >
             	<div className="panel-heading">
             		<h3 className="panel-title">
             			<div className="row">
             				<div className="col-md-12 col-xs-4" >{this.props.name}</div>      		                 		 
             			</div>         		    
             		</h3>
             	</div>
             	<div className="panel-body">
             		<div className="row">                  		    
             			<div className="col-md-1 col-xs-2"><strong>Number</strong></div>
             			<div className="col-md-1 col-xs-2"><strong>Name</strong></div>
             			<div className="col-md-2 col-xs-4"><strong>Description</strong></div>
             			<div className="col-md-1 col-xs-2"><strong>ECTS</strong></div>
             			<div className="col-md-2 col-xs-4"><strong>Edition</strong></div>
             			<div className="col-md-2 col-xs-4"><strong>Enrolled students</strong></div>             			             		 
             		</div><br/> 
             		<div className="row">	
             				<div className="col-md-1 col-xs-2">{this.props.number}</div>
	    	      		 	<div className="col-md-1 col-xs-2">{this.props.name}</div>
	    	      		 	<div className="col-md-2 col-xs-4">{this.props.description}</div>
	    	      		 	<div className="col-md-1 col-xs-2">{this.props.ects}</div>
	    	      		 	<div className="col-md-2 col-xs-4">{this.props.edition}</div>
	    	      		 	<div className="col-md-2 col-xs-4"><Link to={`/dashboard/`+ this.props.prof + '/' + this.props.number }>enrolled students</Link></div>
	    	      	</div><br/>      		  
             	</div>
             		<div className="panel-footer panel-danger"></div>
            </div>  				
		 )
	}
})


var DashboardEnrolled = React.createClass({
	getInitialState: function() {
	    return {enrolled: [],studentNumber:0,students:[]};
	  },	
	  addStudent: function(){
		  console.log(this.props.params.number);
		  console.log(this.state.studentNumber);
		    $.ajax({
		    	  url: "/courses/"+this.props.params.number+"/"+this.state.studentNumber+"/add",
			      dataType: 'json',
			      cache: false,
			      success: function(data) {
			        this.setState({enrolled: data});
			        //console.log("aa");
			        //console.log(this.state.enrolled);
			      }.bind(this),
			      error: function(xhr, status, err) {
			    	  console.log("jou zle");
			    	  console.error(this.props.url, status, err.toString());
			      }.bind(this)
			    });	
	  },
	  changeStudentNumber: function(e) {
          this.setState({studentNumber: e.target.value});
	  },
	componentDidMount: function() {	    
	    $.ajax({
		      url: "/courses/"+this.props.params.number+"/enrolled",
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		        this.setState({enrolled: data});
		        //console.log("aa");
		        //console.log(this.state.enrolled);
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	  console.log("bb");
		    	  console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	    $.ajax({
		      url: "/students",
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		        this.setState({students: data});
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	  console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	  },
	render: function(){		 
		console.log(this.state.enrolled);
		return(
				<div>
				<h1>Enrolled students</h1>
				 {this.state.enrolled.map(function(s) {
				 return	<div><p>{s.name} - {s.studentnumber}<Link to={`/dashboardMarks/`+ s.name + "/"+s.currentCourse }> Grades </Link><Link to={''}> Delete</Link></p></div>;
				 })}
		
				 <select className="selectSt" onChange={this.changeStudentNumber}>
				 	{this.state.students.map(function(stud) {
					 return <option value={stud.studentnumber}>{stud.name}</option>;
					 })}
				 </select>
				 <button type="button" className="btn btn-primary" onClick={this.addStudent}>Add student</button>
                </div>
		 )
	}
})

var ProfCourses = React.createClass({
	 render: function(){		 
		 return(			 			 
				 <li>{this.props.name}</li> 				 				 
		 		)
	}
})


var Marks = React.createClass({	
	
	
	getInitialState: function() {
	    return {marks: [], edit:false, exam:"", project:"", test:""};
	  },
	  
	  activeEdit: function(e) {
          e.preventDefault();
            this.setState({ edit: true });     
    },
    
    deactiveEdit: function(e) {
        e.preventDefault();
       
        if(this.state.exam == '')  this.state.exam = this.state.marks[0];
	  	if(this.state.project == '') this.state.project = this.state.marks[1];
	  	if(this.state.test == '') this.state.test = this.state.marks[2];
	  	e.preventDefault();
		
		
		
		
		this.submit();
        this.setState({ edit: false });   
    },
    
    submit: function() {
	    $.ajax({
		      url: "/courses/"+this.props.params.Sname+"/" + this.props.params.Cname + "/"+this.state.exam + "/"+this.state.project+ "/"+this.state.test,
		      type: "PUT",		      
		      contentType: 'application/json',
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		    	  console.log('hura');
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	},
    
    
	  
	componentDidMount: function() {	    
	    $.ajax({
		      url: "/courses/marks/"+this.props.params.Sname+"/" + this.props.params.Cname,
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		        this.setState({marks: data});
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });	  	 	    
	  },
	  
	  
	  changeExam: function(e) {
          this.setState({exam: e.target.value});
	  },
	  changeProject: function(e) {
	          this.setState({project: e.target.value});
	  },
	  changeTest: function(e) {
	          this.setState({test: e.target.value});
	  },
	
	
	
	render: function() {
		var show;
        if (!this.state.edit){ 
        	show =  <div>		
			            <h3 className="subjectsList">Grades:</h3>
						<ul className="subjUl">
			     			<li>Exam: {this.state.marks[0]}</li>
			     			<li>Project: {this.state.marks[1]}</li>
			     			<li>Test: {this.state.marks[2]}</li>
			     			<button type="button" className="btn btn-primary" onClick={this.activeEdit}>Edit</button>
			     		</ul> 
			     		 
		            </div>        	
        }      
        else{
        	show = <div>		
            <h3 className="subjectsList">Grades:</h3>
			<ul className="subjUl">
     			<li>Exam: <input type="text" name="exam" defaultValue={this.state.marks[0]} onChange={this.changeExam}/></li>
     			<li>Project: <input type="text" name="project" defaultValue={this.state.marks[1]} onChange={this.changeProject}/></li>
     			<li>Test: <input type="text" name="test" defaultValue={this.state.marks[2]} onChange={this.changeTest}/></li>
     			<button type="button" className="btn btn-primary" onClick={this.deactiveEdit}>Save</button>
     		</ul> 
     		
        </div> 
        	
        }
		
		console.log(this.state.marks);
		return (   
                <div>      
               {show}
               </div>
            )
    }
});


var ProfName = React.createClass({
    render: function() {
        return (
            <strong>Doctor Strange</strong>
            )
    }
});

 
 var ProfInfo = React.createClass({
	 getInitialState: function() {      
	        return {name: '', department: '', edit:false,alert:''};
	    }, 
	 activeEdit: function(e) {
         e.preventDefault();
           this.setState({ alert: '' });
           this.setState({ edit: true });    
	 },
	 deactiveEdit: function(e) {
		 if(this.state.name == '') this.state.name = this.props.name;
		 if(this.state.department == '') this.state.department = this.props.department;
		 var proff = {
					id: this.props.id,
					name: this.state.name,
					department:this.state.department
			};
		 this.submit(proff);
         this.setState({ edit: false });    
	 },
	submit(professor) {
		    $.ajax({
			      url: "/professors",
			      type: "PUT",
			      data: JSON.stringify(professor),
			      contentType: 'application/json',
			      dataType: 'json',
			      cache: false,
			      success: function(data) {
			    	  console.log('huraprof');
			    	  this.setState({ alert: 'Edit successful' });
			      }.bind(this),
			      error: function(xhr, status, err) {
			        console.error(this.props.url, status, err.toString());
			      }.bind(this)
			    });
	 },
	 changeName: function(e) {
         this.setState({name: e.target.value});
	 },
	 changeDepartment: function(e) {
         this.setState({department: e.target.value});
	 },
	 render: function(){
		 if(this.state.edit){
			 return(
					 <div className="professor-info">
	                 <h2><input className="inp" type="text" defaultValue={this.props.name} onChange={this.changeName}></input></h2>
	                     <figure style={{marginRight: 1 + 'em'}}>
	                         <img src="/assets/img/avatar2.png" alt="Default avavatar" width="150" height="150" />                       
	                        	 <br/> <br/>  
	                         <span><strong>Department: </strong>
	                         <select onChange={this.changeDepartment}>
	                         	<option value={this.props.department}>{this.props.department}</option>
	                         	<option value="INFORMATICS">INFORMATICS</option>
	                         	<option value="MATHEMATICS">MATHEMATICS</option>
	                         	<option value="CHEMISTRY">CHEMISTRY</option>
	                        	<option value="BIOLOGY">BIOLOGY</option>
	                         </select>
	                         </span><br/>
	                         <span><strong>Role: </strong>{this.props.role}</span>      
	                      </figure><br/>
	                      <button type="button" className="btn btn-primary" onClick={this.deactiveEdit}>Save</button>
	                 </div>
			 )
		 }
		 else{
			 return(
					 <div className="professor-info">
	                 <h2>{this.props.name}</h2>
	                     <figure style={{marginRight: 1 + 'em'}}>
	                         <img src="/assets/img/avatar2.png" alt="Default avavatar" width="150" height="150" />                       
	                        	 <br/> <br/>  
	                         <span><strong>Department: </strong>{this.props.department}</span><br/>
	                         <span><strong>Role: </strong>{this.props.role}</span>      
	                      </figure><br/>
	                      <button type="button" className="btn btn-primary" onClick={this.activeEdit}>Edit</button>
	                 </div>
			 )
		 }
	}
 })
 

var ProfDetails = React.createClass({
	getInitialState: function() {
	    return {profil: [], profCourses: []};
	  },
	  
	componentDidMount: function() {
	    $.ajax({
	      url: "/professors/"+this.props.params.username,
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	        this.setState({profil: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	    $.ajax({
		      url: "/professors/"+this.props.params.username+"/courses",
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		        this.setState({profCourses: data});
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });	    
	  },
	
	
	render: function () {
		var courseNodes = this.state.profCourses.map(function(course) {
			return (
					<ProfCourses key={course.id} name={course.name}/>
			);
		});
		
    	return (
        		<div>
        		
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <h1>My profile</h1>
                        </div>
                    </div>
     
                    <div className="row">
                        <div className="col-md-4 subjects">
                            <ProfInfo id={this.state.profil.id} name={this.state.profil.name} department={this.state.profil.department} role={this.state.profil.role}/>                            
                        </div>
                            
                        <div className="col-md-8">
                        <h3 className="subjectsList">My courses: </h3>
            				<ul className="subjUl">{courseNodes}</ul>                         
                        </div>
                    </div>                    
                    
                </div>

                </div>
           
            )
    }
});

