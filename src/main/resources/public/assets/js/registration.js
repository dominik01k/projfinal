var Registration = React.createClass({
    render: function() {
        return (
        	<div>
        		<NavSignIn/>
        		<RegisterForm />
           </div>
            )
    }
});


var RegisterForm = React.createClass({
	
	getInitialState: function() {      
        return {uni_email: '', alert:'', name: '', student_number: '', password: '', addres: '', degree: '', email: '', photo: ''};
    },
    changeName: function(e) {
    	var char;
    	this.setState({name: e.target.value});
    	var name = e.target.value.replace(/ /g, "");
    	var uni_mail = name + "@fct.unl.pt";
    	var hash = 0;
    	if (e.target.value.length == 0) return hash;
    		for (var i = 0; i < e.target.value.length; i++) {
    			char = e.target.value.charCodeAt(i);
    			hash = hash+char;
    			hash = hash & hash; // Convert to 32bit integer
    		}
    	var stringHash = hash.toString();
        this.setState({uni_email: uni_mail});
        this.setState({student_number: stringHash});
    },
    changePassword: function(e) {
        this.setState({password: e.target.value});
    },
    changePassword: function(e) {
        this.setState({password: e.target.value});
    },
    changeEmail: function(e) {
        this.setState({email: e.target.value});
    },
    changeAddres: function(e) {
        this.setState({addres: e.target.value});
    },
    changeDegree: function(e) {
        this.setState({degree: e.target.value});
    },
    changePhoto: function(e) {
        this.setState({photo: e.target.value});
    },
    
	handleSubmit(e) {
		e.preventDefault();
		var newStudent = {
				name: this.state.name,
				studentnumber: this.state.student_number,
				email: this.state.email,
				uni_email: this.state.uni_email,
				degree: this.state.degree,
				photo: 'avatar1.png',
				password: this.state.password,
				addres: this.state.addres	
		};
		console.log(newStudent);
		this.submit(newStudent);
	},
	
	submit(student) {
		console.log(student);
	    $.ajax({
		      url: "/students",
		      type: "POST",
		      data: JSON.stringify(student),
		      contentType: 'application/json',
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		    	  this.setState({alert: 'Registration successful, you can login with your student number'});
		        //student.id = data;
				//this.props.appendStudent(student);
				//this.refs.nameInput.focus();
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	  console.warn(xhr.responseText)
		      }.bind(this)
		    })
	},
	render: function(){
		return (
				<div>
				<div className="alert alert-danger" role="alert">{this.state.alert}</div>
				<form className="form-horizontal" onSubmit={this.handleSubmit}>
			  <div className="form-group">
			    <label htmlFor="student_number" className="col-sm-2 control-label">Student number</label>
			    <div className="col-sm-10">
			      <input type="number" className="form-control" id="student_number" value={this.state.student_number} disabled />
			    </div>
			  </div>
			    <div className="form-group">
			    <label htmlFor="student_uni_mail" className="col-sm-2 control-label">Student university email</label>
			    <div className="col-sm-10">
			      <input type="text" className="form-control" id="student_uni_mail" value={this.state.uni_email} disabled />
			    </div>
			  </div>
			  
			  <div className="form-group">
			    <label htmlFor="student_name" className="col-sm-2 control-label">Full Name</label>
			    <div className="col-sm-10">
			      <input type="text" className="form-control" id="student_name" placeholder="full name" onChange={this.changeName} />
			    </div>
			  </div>
			  <div className="form-group">
			    <label htmlFor="student_password" className="col-sm-2 control-label">Password</label>
			    <div className="col-sm-10">
			      <input type="password" className="form-control" id="student_password" placeholder="password" onChange={this.changePassword} />
			    </div>
			  </div>
			  <div className="form-group">
			    <label htmlFor="student_email" className="col-sm-2 control-label">Personal email</label>
			    <div className="col-sm-10">
			      <input type="email" className="form-control" id="student_email" placeholder="email" onChange={this.changeEmail} />
			    </div>
			  </div>
			  <div className="form-group">
			    <label htmlFor="student_addres" className="col-sm-2 control-label">Addres</label>
			    <div className="col-sm-10">
			      <input type="text" className="form-control" id="student_addres" placeholder="addres" onChange={this.changeAddres} />
			    </div>
			  </div>
			  <div className="form-group">
			    <label htmlFor="student_photo" className="col-sm-2 control-label">Photo</label>
			    <div className="col-sm-10">
			      <input type="file" className="form-control" id="student_photo" placeholder="photo" onChange={this.changePhoto} />
			    </div>
			  </div>
			  <div className="form-group">
			    <label htmlFor="student_degree" className="col-sm-2 control-label">Degree</label>
			    <div className="col-sm-10">
				    <select className="form-control" id="student_degree" onChange={this.changeDegree}>
				    <option></option>
				    <option>Informatics</option>
				    <option>Mathematics</option>
				    <option>Chemistry</option>
				  </select>
			    </div>
			  </div>
		
			  <div className="form-group">
			    <div className="col-sm-offset-2 col-sm-10">
			      <button type="submit" className="btn btn-default">Register</button>
			    </div>
			  </div>
			  </form>
			  </div>				
			)
	}
});