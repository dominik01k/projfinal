/**
 * 
 */

var StudentName = React.createClass({
    render: function() {
        return (
            <strong>Joao Couto</strong>
            )
    }
});



var StudentCourses = React.createClass({
	getInitialState: function() {      
        return {courses: [],showGrades:false,grades:[],have:false};
    },
    componentDidUpdate: function(){
    	if(!this.state.have){
    		this.getCourses();    		
    	}
    },
    getCourses(){
	    $.ajax({
	    	url: "/students/profile/"+this.props.studentnumber+"/courses",
	      cache: false,
	      dataType: 'json',
	      success: function(data) {
	        this.setState({courses: data});
	        this.setState({have: true});
	      }.bind(this),
	      error: function(xhr, status, err) {
	    	console.log('chyba nemam kurzy');
	        //console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	  },
	getGrades: function(name,studentnumber){
		$.ajax({
		      url: "/courses/get/"+name+"/"+studentnumber,
		      cache: false,
		      dataType: 'json',
		      success: function(data) {
		        this.setState({grades: data});
		        this.setState({showGrades: true});
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	console.log('chyba nemam znamky');
		        //console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	},
	backToCourses: function(){
		this.setState({showGrades: false});
	},
	render: function() {
		if(this.state.showGrades){
			return (
					<div>
						<h3 className="subjectsList">Grades:</h3>
						<ul className="subjUl">
	             			<li>Exam: {this.state.grades[0]}</li>
	             			<li>Project: {this.state.grades[1]}</li>
	             			<li>Test: {this.state.grades[2]}</li>
	             		</ul>
	             		<center><button className="backTo" onClick={this.backToCourses}>Back to courses list</button></center>
					</div>
				)
		}
		else{
			return (
					<div>
						<h3 className="subjectsList">My active subjects:</h3>
						<ul className="subjUl">
							<li><a onClick={() => this.getGrades(this.state.courses[0],this.props.studentnumber)}>{this.state.courses[0]}</a>
							</li>
							<li><a onClick={() => this.getGrades(this.state.courses[1],this.props.studentnumber)}>{this.state.courses[1]}</a>
							</li>
							<li><a onClick={() => this.getGrades(this.state.courses[2],this.props.studentnumber)}>{this.state.courses[2]}</a>
							</li>
							<li><a onClick={() => this.getGrades(this.state.courses[3],this.props.studentnumber)}>{this.state.courses[3]}</a>
							</li>
							<li><a onClick={() => this.getGrades(this.state.courses[4],this.props.studentnumber)}>{this.state.courses[4]}</a>
							</li>
							<li><a onClick={() => this.getGrades(this.state.courses[5],this.props.studentnumber)}>{this.state.courses[5]}</a>
							</li>
						</ul>
					</div>
		            )
		}

    }
});
 
var StudentInfo = React.createClass({
	
	getInitialState: function() {      
        return {number:'', email: '', addres: '', edit: false};
    },
    activeEdit: function(e) {
          e.preventDefault();
            this.setState({ edit: true });     
    },
    deactiveEdit: function(e) {
            e.preventDefault();
            console.log(this.state.number);
            if(this.state.number == '')  this.state.number = this.props.number;
		  	if(this.state.email == '') this.state.email = this.props.email;
		  	if(this.state.addres == '') this.state.addres = this.props.addres;
		  	e.preventDefault();
			var newStudent = {
					id: this.props.id,
					name: this.props.name,
					studentnumber:this.state.number,
					email: this.state.email,
					addres: this.state.addres
			};
			this.submit(newStudent);
            this.setState({ edit: false });   
    },
	submit(student) {
	    $.ajax({
		      url: "/students",
		      type: "PUT",
		      data: JSON.stringify(student),
		      contentType: 'application/json',
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		    	  console.log('hura');
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	},
    changeTel: function(e) {
            this.setState({number: e.target.value});
    },
    changeMail: function(e) {
            this.setState({email: e.target.value});
    },
    changeAdr: function(e) {
            this.setState({addres: e.target.value});
    },
   
    render: function() {
    	var show;
            if (!this.state.edit){
                show =  <div className="student-info">
                    <h2 className="studentName">{this.props.name}</h2>
                        <figure style={{marginRight: 1 + 'em'}}>
                            <img  className="profilePhoto" src={"assets/img/" + this.props.photo} alt="Default avavatar" width="100" height="100" />
                            <figcaption className="studentIdName">{this.props.name} ( {this.props.number} )</figcaption>
                            <ul className="studentDetail">
                            <li><span>Degree: {this.props.degree}</span></li>
                            <li><span>University email: {this.props.uni_email}</span></li>         
                            <li><span>Email: {this.props.email}</span></li>
                            <li><span>Adress: {this.props.addres}</span></li>
                            </ul>
                         </figure>
                         <button type="button" className="btn btn-primary" onClick={this.activeEdit}>Edit</button>
                    </div>
            }
            else{
                 show = <div className="student-info">
                    <h2 className="studentName">{this.props.name}</h2>
                        <figure style={{marginRight: 1 + 'em'}}>
                            <img src={"assets/img/" + this.props.photo} className="profilePhoto" alt="Default avavatar" width="100" height="100" />
                            <figcaption className="studentIdName">{this.props.name} ( {this.props.number} )</figcaption>
                            <ul className="studentDetail">
                            <li><span>Degree: {this.props.degree}</span></li>
                            <li><span>University email: {this.props.uni_email}</span></li>    
                            <li>Email: <input type="text" name="mail" defaultValue={this.props.email} onChange={this.changeMail} /></li>
                            <li>Addres: <input type="text" name="addres" defaultValue={this.props.addres} onChange={this.changeAdr} /></li>
                            </ul>
                         </figure>
                         <button type="button" className="btn btn-primary" onClick={this.deactiveEdit}>Save</button>
                    </div>
                   
            }
           
          return(                
                    <div>            
                    {show}      
                   </div>              
            );
    }
           
}); 
 
 
var StudentDetails = React.createClass({	
	getInitialState: function(){	
		return {
			profil : []
		}
	},
	componentDidMount: function() {
	    $.ajax({
	    	url: "/students/profile/"+this.props.params.id,
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	        this.setState({profil: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	    	console.log('chyba');
	        //console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	  },
	  
	render: function () {
		console.log(this.state.profil);
		return (
        		 <div>
                <div className="container-fluid">
                
 
                <div className="row">
                    <div className="col-md-12">
                        <h1>My profile</h1>
                    </div>
                </div>
 
                <div className="row">
                    <div className="col-md-4 subjects">
                        <StudentInfo id={this.state.profil.id} name={this.state.profil.name}
                        number={this.state.profil.studentnumber} email={this.state.profil.email}
                        addres={this.state.profil.addres} photo={this.state.profil.photo} uni_email={this.state.profil.uni_email}
                        degree={this.state.profil.degree} />
                    </div>
                        
                    <div className="col-md-8">
                    	<StudentCourses studentnumber={this.state.profil.studentnumber} />
                    </div>

                </div>
            </div>
            </div>           
            )
    }
});
