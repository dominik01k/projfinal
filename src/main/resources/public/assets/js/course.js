
var EnrolledStudentInfo = React.createClass({
	
	getInitialState: function(){	
		console.log('loaded');
		return {
			profil : []
		}
	},

	componentDidMount: function() {
	    $.ajax({
	    	url: "/students/"+this.props.params.id,
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	        this.setState({profil: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	    	console.log('chyba');
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	  },
	  
	render: function () {
		console.log();
		return (
				<div className="row">			
				<div className="col-md-1">{this.props.number}</div>
				<div className="col-md-1">{this.props.name}</div>
				<div className="col-md-2">{this.props.description}</div>
				<div className="col-md-1">{this.props.ects}</div>
				<div className="col-md-1"><Link to={'/enrolledStudents/'+ this.props.number}>enrolledStudents</Link></div>
				</div>
        		 
            )
    }
});



var EnrolledStudents = React.createClass({
	getInitialState: function(){	
		console.log('loaded');
		return {
			course : [], students:[], enrolled:[14],cislo:[],data:[],
		}
	},

	componentDidMount: function() {
	    $.ajax({
	    	url: "/courses/"+this.props.params.number,
	      dataType: 'json',
	      cache: false,
	      success: function(data) {	  
	    	  console.log("somt tu");
	        this.setState({course: data});   
	        this.setState({enrolled: data.enrolled});       
	      }.bind(this),
	      error: function(xhr, status, err) {
	    	console.log('chybaaaaaaaaa');
	        console.error(this.props.url, status, err.toString());
	      }.bind(this),  	      
	      
	    }); 
	    
	    
		    $.ajax({
		      url: "/students/druhy/"+this.state.enrolled,
		      dataType: 'json',
		      cache: false,
		      success: function(data) {
		    	 
		        this.setState({data: data});
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
		  
	    
	    
	    
	  },
	
	 
	render: function() {
		console.log("enrolled students ARE");
		console.log(this.state.enrolled);
		console.log("courses");
		console.log(this.state.data);
		 var namesList = this.state.enrolled.map(function(n){
             return <CourseDetails2 key={n} number={n}/>;
           })
		 
		
		
		return (	
				
				
				
			<div>	
			<h1>Blaa</h1>
		   
			</div>
		)
	}
});

var CourseDetails2 = React.createClass({
	getInitialState: function(){	
		console.log('loaded');
		return {
			student:[], cislo:12,
		}
	},

	componentDidMount: function() {
	    $.ajax({
	    	url: "/students/druhy/"+this.props.number,
	      dataType: 'json',
	      cache: false,
	      success: function(data) {	  
	    	  console.log("somt tu");
	        this.setState({student: data});        
	      }.bind(this),
	      error: function(xhr, status, err) {
	    	console.log('chybaaaaaaaaa');
	        console.error(this.props.url, status, err.toString());
	      }.bind(this),  	      
	      
	    });      
	  },

	render: function() {
		return (			
			
				<h2>{this.props.number}</h2>	
		)
	}
});



var CourseDetails = React.createClass({
	render: function() {
		return (			
				
			<div className="row">			
			<div className="col-md-1">{this.props.number}</div>
			<div className="col-md-1">{this.props.name}</div>
			<div className="col-md-2">{this.props.description}</div>
			<div className="col-md-1">{this.props.ects}</div>
			<div className="col-md-1"><Link to={'/enrolledStudents/'+ this.props.number}>enrolledStudents</Link></div>
			</div>
			
		)
	}
});


var CompleteCourses = React.createClass({
	render: function() {
		return (
			<div>
			<CourseDetails number={this.props.number} name={this.props.name} description={this.props.description} ects={this.props.ects}/>
			</div>
		)
	}
})


var TeacherCourses = React.createClass({
	getInitialState: function() {
		console.log('loaded');
	    return {data: []};
	  },
	  
	componentDidMount: function() {
	    $.ajax({
	      url: "/courses/teacherCourses",
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	    	  console.log(data);
	        this.setState({data: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	  },
	
	render: function() {
		var courseNodes = this.state.data.map(function(course) {
			return (
					<CompleteCourses key={course.id} number={course.number}
					 name={course.name} description={course.description} ects={course.ects}/>
			);
		});
    return (
      <div className="container">
      	<h1>List o your courses</h1>
      	
      	
      	<div>
      	<div className="row">			
        <div className="col-md-1">Number</div>
        <div className="col-md-1">Name</div>
        <div className="col-md-2">Description</div>
        <div className="col-md-1">ECTS</div>
		</div>
		<br/>
		</div>
        {courseNodes}
       
      </div>
    );
  },
  
 
});

