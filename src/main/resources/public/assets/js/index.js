
var Home = React.createClass({
    render: function() {
        console.log("tu som");
    	return (
        	<div>
           <Jumbotron/>
           <Table/>
           </div>
           
            )
    }
});

var HomeLogged = React.createClass({
    render: function() {
        console.log("tu som");
    	return (
        	<div>
           <Jumbotron2/>
           <Table/>
           </div>
           
            )
    }
});

var NavSignIn = React.createClass({
    render: function() {
    	return (
    			<nav className="navbar navbar-inverse">
    			  <div className="container-fluid">
    			   
    			    <div className="navbar-header">
    			      <a className="navbar-brand" href="#">CLIP</a>
    			    </div>			  	   
    			  </div>
    			</nav>         
    	)
    }
});

var Jumbotron = React.createClass({	
	  render: function(){    	
    	return (    			 
    		<div>
    		<div className="jumbotron" style={{paddingLeft:50}}>    			
  			  <h2><strong>Welcome in a CLIP!</strong></h2>
  			  <p>If you are not registered yet, don't hesitate, sing up and start use CLIP.</p>
  			  <p><Link className="btn btn-danger" to={'/register'} >Sign up</Link></p>    			
  			 </div>  				 
  		  </div>    
        );
    }
});

var Jumbotron2 = React.createClass({	
	  render: function(){    	
  	return (    			 
  		<div>
  		<div className="jumbotron" style={{paddingLeft:50}}>    			
			  <h2><strong>Welcome in a CLIP!</strong></h2>
			  <p>Hello. Welcome back</p>   			
			 </div>  				 
		  </div>    
      );
  }
});
var Degree = React.createClass({	
	render: function(){
		return(				 
             	<div className="row">
             	<div className="col-md-2 col-xs-4">{this.props.name}</div>
             	<div className="col-md-4 col-xs-6">{this.props.description}</div>
             	<div className="col-md-1 col-xs-2">{this.props.ects}</div>
             	<div className="col-md-1 col-xs-2">{this.props.years}</div>
             	<div className="col-md-4 col-xs-6">{this.props.courses}</div>              		 
             	</div>		
		
		)
	}
	
});

var Table = React.createClass({	
	getInitialState: function(){
		return { degrees:[] }		
	},
	
	componentDidMount: function() {
	    $.ajax({
	    	url: "/degrees",
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	        this.setState({degrees: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	    	console.log('chyba');
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	  },		
	
	render: function(){
		var degreeNodes = this.state.degrees.map(function(degree) {
			return (
					<Degree key={degree.id} name={degree.name}
					description={degree.description} ects={degree.ects} years={degree.years} courses={degree.courses}/>
			);
		});
		console.log(this.state.degrees);       
		return (  
				<div style={{marginLeft: 50,marginRight:50}}>	
         		<h4><strong>Here you can see list of available degrees:</strong></h4>
         		<br/>
         		<div className="panel panel-success" >
         		  <div className="panel-heading">
         		    <h3 className="panel-title">
         		    <div className="row">
             		  <div className="col-md-2 col-xs-4">Name</div>
             		  <div className="col-md-4 col-xs-6">Description</div>
             		  <div className="col-md-1 col-xs-2">ECTS</div>
             		  <div className="col-md-1 col-xs-2">Years</div>
             		  <div className="col-md-4 col-xs-6">Courses</div>                		 
             		</div>         		    
         		    </h3>
         		  </div>
         		  <div className="panel-body">
                	{degreeNodes} 
                  </div>
            	  <div className="panel-footer panel-success"></div>
            		</div>   
            		</div> 
                );
            }
        });
