/**
 * 
 */

var Link = ReactRouter.Link;
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;


ReactDOM.render((
		<Router>
	    	<Route path="/" component={Home}/>	    	
	    	<Route path="/welcome" component={HomeLogged}/>
	    	<Route path="/register" component={Registration}/>
	    	<Route path="/profile/:id" component={StudentDetails}/>
	    	<Route path="/dashboard/:username" component={Dashboard}/>
	    	<Route path="/dashboard/:username/:number" component={DashboardEnrolled}/>
	    	<Route path="/dashboardMarks/:Sname/:Cname" component={Marks}/>
	    	<Route path="/profileProf/:username" component={ProfDetails}/>	    	
	    	<Route path="/teacherCourses" component={TeacherCourses}/>
	    	<Route path="/enrolledStudents/:number" component={EnrolledStudents}/>
	    			
	    			
	    			
	    	</Router>
		), document.getElementById('main'))